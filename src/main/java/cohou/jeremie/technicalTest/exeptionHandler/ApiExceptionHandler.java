package cohou.jeremie.technicalTest.exeptionHandler;

import cohou.jeremie.technicalTest.annotation.LogExceptionHandler;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * User api rest handler controller
 * intercept thrown errors and return ErrorMessage with proper HttpStatus
 */
@RestControllerAdvice
public class ApiExceptionHandler {

    /*Unused method handler exception, deprecated was throws before when validation wasn't call with annotation*/
    @ExceptionHandler(value = PostValidationException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @LogExceptionHandler
    public ErrorMessage validatorExeption(PostValidationException ex) {
        String detailMessage = ExceptionUtils.extractDetailedMessage(ex);
        return new ErrorMessage(HttpStatus.BAD_REQUEST.value(), new Date(), ex.getMessage(), "Bad Request");
    }

    @ExceptionHandler(value = ResourceNotFoundException.class)
    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    @LogExceptionHandler
    public ErrorMessage resourceNotFoundException(ResourceNotFoundException ex) {
        String detailMessage = ExceptionUtils.extractDetailedMessage(ex);
        return new ErrorMessage(HttpStatus.NOT_FOUND.value(), new Date(), ex.getMessage(), "Resource not found");
    }

    /*@ExceptionHandler(value = ConstraintViolationException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @LogExceptionHandler
    public ErrorMessage ConstraintViolationException(ConstraintViolationException ex) {

        Set<ConstraintViolation<?>> violations = ex.getConstraintViolations();

        Map<String, String> errors = new HashMap<>();
        violations.forEach((error) -> {
            String fieldName = error.getPropertyPath().toString();
            String message = error.getMessage();
            errors.put(fieldName, message);
        });

        return new ErrorMessage(HttpStatus.BAD_REQUEST.value(), new Date(), errors.toString(), "Bad Request");
    }*/

    @ExceptionHandler(value = HttpMessageNotReadableException.class)
    @LogExceptionHandler
    public ErrorMessage HttpMessageNotReadableException(HttpMessageNotReadableException ex){
        String detailMessage = ExceptionUtils.extractDetailedMessage(ex);
        return new ErrorMessage(HttpStatus.BAD_REQUEST.value(), new Date(), detailMessage, "Bad Request");
    }

    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    @LogExceptionHandler
    public ErrorMessage MethodArgumentNotValidException(MethodArgumentNotValidException ex){
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return new ErrorMessage(HttpStatus.BAD_REQUEST.value(), new Date(), errors.toString(), "Bad Request");
    }
}

