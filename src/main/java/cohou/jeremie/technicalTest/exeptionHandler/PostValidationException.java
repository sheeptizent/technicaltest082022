package cohou.jeremie.technicalTest.exeptionHandler;

/*Unused class exception, deprecated was throws before when validation wasn't call with annotation*/
public class PostValidationException extends RuntimeException {
    public PostValidationException(String message) {
        super(message);
    }
}
