package cohou.jeremie.technicalTest.exeptionHandler;

import org.apache.commons.lang3.StringUtils;

public class ExceptionUtils {

    static String extractDetailedMessage(Throwable e) {

        final String message = e.getMessage();

        if (message == null) {
            return "";
        }
        final int tailIndex =  StringUtils.indexOf(message, "; nested exception is");
        if (tailIndex == -1) {
            return message;
        }
        return StringUtils.left(message, tailIndex);
    }

}
