package cohou.jeremie.technicalTest.configuration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.StringJoiner;

@Component
public class Interceptor implements HandlerInterceptor {

    private static Logger logger = LoggerFactory.getLogger(Interceptor.class);
    private final String toServerRequest = "====> PREHANDLE REQUEST";

    @Override
    public boolean preHandle
            (HttpServletRequest request, HttpServletResponse response, Object handler)
            throws Exception {
        StringJoiner joiner = new StringJoiner("\n", "\n\n", "\n");
        joiner.add(toServerRequest);
        joiner.add("Calling for : " + request.getMethod() + " " + request.getRequestURI());
        logger.info(joiner.toString());
        return true;
    }

    @Override
    public void afterCompletion
            (HttpServletRequest request, HttpServletResponse response, Object
                    handler, Exception exception) throws IOException {
    }
}
