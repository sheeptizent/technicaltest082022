package cohou.jeremie.technicalTest.mapper;

import cohou.jeremie.technicalTest.model.entity.User;
import cohou.jeremie.technicalTest.model.restBean.UserRestBean;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface UserMapper {

    /**
     * map userResource to User entity
     *
     * @param form the UserResource to map
     * @return User entity
     */
    User toEntity(UserRestBean form);

    /**
     * map User entity to userResource
     *
     * @param user the User entity to map
     * @return UserResource
     */
    UserRestBean toResource(User user);
}
