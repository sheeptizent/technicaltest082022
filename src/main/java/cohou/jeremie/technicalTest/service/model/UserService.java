package cohou.jeremie.technicalTest.service.model;

import cohou.jeremie.technicalTest.exeptionHandler.ResourceNotFoundException;
import cohou.jeremie.technicalTest.model.entity.User;
import cohou.jeremie.technicalTest.repository.UserRepository;
import org.springframework.stereotype.Service;

import static java.lang.String.format;

/**
 * Couche service de l'entite User
 */
@Service
public class UserService {

    private UserRepository userRepository;

    public UserService(UserRepository userRepository){
        this.userRepository = userRepository;
    }

    /**
     * Sauvegarde d'un utilisateur
     * @param user
     * @return User
     */
    public User save(User user) {
        return userRepository.save(user);
    }

    /**
     * Retourne un utilisateur pour un id donné
     * @param id
     * @return User | null
     */
    public User findById(Long id) {
        return  userRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException(format("User with ID [%s], not found", id)));
    }

}
