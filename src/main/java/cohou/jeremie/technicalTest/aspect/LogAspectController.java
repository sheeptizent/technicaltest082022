package cohou.jeremie.technicalTest.aspect;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;
import org.springframework.web.bind.annotation.RequestMapping;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.StringJoiner;

@Component
@Aspect
public class LogAspectController {

    private static final Logger logger = LoggerFactory.getLogger(LogAspectController.class);

    private final ObjectMapper mapper;

    private final String exFailed = "execution failed";
    private final String getRequestInf = "---- request information ----";
    private final String toServerRequest = "====> REQUEST";
    private final String fromServerRequest = "<==== RESPONSE";

    public LogAspectController(ObjectMapper mapper) {
        this.mapper = mapper;
    }

    @Pointcut("execution(* cohou.jeremie.technicalTest.controller.*.*.*(..))")
    public void pointCut() {
        /*pointCut method*/
    }

    @Before("pointCut()")
    public void logMethodBefore(JoinPoint joinPoint) throws JsonProcessingException {
        StringJoiner joinerBefore = new StringJoiner("\n", "\n\n", "\n");
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        RequestMapping mapping = signature.getMethod().getAnnotation(RequestMapping.class);
        Map<String, Object> parameters = ToolAspect.getParameters(joinPoint);

        joinerBefore.add(toServerRequest);
        joinerBefore.add(getRequestInf);

        try {
            var parametersAsString = mapper.writeValueAsString(parameters);
            joinerBefore.add("request time : " + new SimpleDateFormat("yyyy MM dd HH:mm:ss").format(new Date()));
            joinerBefore.add("requestURI : " + mapping.path());
            joinerBefore.add("method type : " + mapping.method());
            joinerBefore.add("request parameters : " + "\n" + parametersAsString);
            logger.info(joinerBefore.toString());
        } catch (JsonProcessingException throwable) {
            joinerBefore.add(exFailed);
            joinerBefore.add(throwable.getMessage());
            logger.error(joinerBefore.toString());
            throw (throwable);
        }
    }

    /**
     * log des informations sur les requetes
     *
     * @param joinPoint
     * @return
     * @throws Throwable
     */
    @Around("pointCut()")
    public Object logExecutionTime(ProceedingJoinPoint joinPoint) throws Throwable {
        final StopWatch stopWatch = new StopWatch();

        stopWatch.start();

        Object proceed = joinPoint.proceed();

        stopWatch.stop();

        logger.info("\n==> EXECUTIONTIME <==:  \"{}\" executed in {} ms", joinPoint.getSignature(), stopWatch.getTotalTimeMillis());

        return proceed;
    }

    @AfterReturning(pointcut = "pointCut()", returning = "entity")
    public void logMethodAfter(JoinPoint joinPoint, ResponseEntity<?> entity) throws JsonProcessingException {
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        RequestMapping mapping = signature.getMethod().getAnnotation(RequestMapping.class);
        StringJoiner joinerAfter = new StringJoiner("\n", "\n\n", "\n");
        joinerAfter.add(fromServerRequest);
        try {
            var entityAsString = mapper.writeValueAsString(entity);
            joinerAfter.add("request time : " + new SimpleDateFormat("yyyy MM dd HH:mm:ss").format(new Date()));
            joinerAfter.add("requestURI : " + mapping.path());
            joinerAfter.add("method type : " + mapping.method());
            joinerAfter.add("return : " + "\n" + entityAsString);
            logger.info(joinerAfter.toString());
        } catch (JsonProcessingException throwable) {
            joinerAfter.add(exFailed);
            joinerAfter.add(throwable.getMessage());
            logger.error(joinerAfter.toString());
            throw (throwable);
        }
    }

}