package cohou.jeremie.technicalTest.aspect;

import cohou.jeremie.technicalTest.exeptionHandler.ErrorMessage;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.StringJoiner;

@Component
@Aspect
public class LogAspectException {

    private static final Logger logger = LoggerFactory.getLogger(LogAspectException.class);
    private final ObjectMapper mapper;
    private final String fromServerRequest = " <===== RESPONSE";

    public LogAspectException(ObjectMapper mapper) {
        this.mapper = mapper;
    }

    @AfterReturning(value = "@annotation(cohou.jeremie.technicalTest.annotation.LogExceptionHandler)", returning = "errorMessage")
    public void afterException(JoinPoint joinPoint, ErrorMessage errorMessage) throws JsonProcessingException {
        StringJoiner joiner = new StringJoiner("\n", "\n\n", "\n");
        joiner.add(fromServerRequest);
        joiner.add(mapper.writeValueAsString(errorMessage));
        logger.error(joiner.toString());
    }

}
