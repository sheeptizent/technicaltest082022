package cohou.jeremie.technicalTest.controller.model;

import cohou.jeremie.technicalTest.exeptionHandler.PostValidationException;
import cohou.jeremie.technicalTest.exeptionHandler.ResourceNotFoundException;
import cohou.jeremie.technicalTest.mapper.UserMapper;
import cohou.jeremie.technicalTest.model.entity.User;
import cohou.jeremie.technicalTest.model.restBean.UserRestBean;
import cohou.jeremie.technicalTest.service.model.UserService;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.validation.*;

/**
 * RestController de l'entite User
 */
@RestController
@RequestMapping()
public class UserController {

    private final UserMapper userMapper;
    private UserService userService;

    public UserController(UserMapper userMapper, UserService userService) {
        this.userMapper = userMapper;
        this.userService = userService;
    }

    /**
     * Endpoint sauvegarde d'un utilisteur
     *
     * @param userBean
     * @throws Exception
     */
    @RequestMapping(path = "/POST/user", method = RequestMethod.POST)
    public ResponseEntity<UserRestBean> create(@Valid @RequestBody UserRestBean userBean) throws RuntimeException {

        var user = this.userMapper.toEntity(userBean);

        User savedUser = userService.save(user);

        UserRestBean userBeanSaved = this.userMapper.toResource(savedUser);

        return new ResponseEntity<>(userBeanSaved, HttpStatus.CREATED);
    }

    /**
     * Endpoint retour d'un user par id
     *
     * @param id
     * @throws Exception
     */
    @RequestMapping(path = "/GET/user/{id}", method = RequestMethod.GET)
    public ResponseEntity<UserRestBean> findById(@PathVariable("id") Long id) throws RuntimeException {
        User user = userService.findById(id);

        UserRestBean userBean = this.userMapper.toResource(user);

        return new ResponseEntity<>(userBean, HttpStatus.OK);
    }

}