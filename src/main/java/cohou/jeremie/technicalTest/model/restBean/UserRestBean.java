package cohou.jeremie.technicalTest.model.restBean;

import cohou.jeremie.technicalTest.model.entity.Country;
import cohou.jeremie.technicalTest.model.entity.Gender;
import cohou.jeremie.technicalTest.model.validator.BirthDateValidator;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Getter
@Setter
public class UserRestBean {

    @Valid
    @NotNull
    private String name;

    @Valid
    @NotNull
    @BirthDateValidator
    private String birthDate;

    @Valid
    @NotNull
    @Enumerated(EnumType.STRING)
    private Country country;

    @Valid
    @Pattern(regexp = "^0[0-9]{9}", message = "doit comprendre 10 chiffres et commencer par 0")
    private String phoneNumber;

    @Valid
    @Enumerated(EnumType.STRING)
    //@EnumValuesValidator(enumClass = Gender.class)
    private Gender gender;

}
