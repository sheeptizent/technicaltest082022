package cohou.jeremie.technicalTest.model.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Logique du BirthDateValidator
 * Check sur la validité de la date ainsi que la majorité légale
 */
public class BirthDateValidatorLogic implements ConstraintValidator<BirthDateValidator, Object> {

    private int legalAge;

    private String dateFormat;

    @Override
    public void initialize(BirthDateValidator birthDateValidator) {
        this.legalAge = birthDateValidator.legalAge();
        this.dateFormat = birthDateValidator.dateFormat();
        ConstraintValidator.super.initialize(birthDateValidator);
    }

    @Override
    public boolean isValid(Object birthDate, ConstraintValidatorContext constraintValidatorContext) {

        if(birthDate == null){
            return true;
        }

        if(!birthDate.getClass().isAssignableFrom(String.class)){
           return false;
        }

        DateFormat sdf = new SimpleDateFormat(this.dateFormat);
        sdf.setLenient(false);
        Date birthDay;
        Date now = new Date();

        try {
            birthDay = sdf.parse((String) birthDate);
        } catch (ParseException e) {
            return false;
        }

        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(birthDay.getTime());
        c.add(Calendar.YEAR, this.legalAge);

        Date majorityDate = c.getTime();

        int sup = majorityDate.compareTo(now);

        if (sup <= 0) {
            return true;
        } else {
            return false;
        }
    }
}
