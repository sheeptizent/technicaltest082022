package cohou.jeremie.technicalTest.model.validator;

import lombok.SneakyThrows;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Logique de ValueOfList
 * Check si la valeur entrante appartient à la liste
 */
public class EnumValuesValidatorLogic implements ConstraintValidator<EnumValuesValidator, CharSequence> {
    private Class<?> classEnum;
    private List<String> acceptedValues = new ArrayList<>();
    private String returnMessage;

    @SneakyThrows
    @Override
    public void initialize(EnumValuesValidator enumValues) {

        classEnum = enumValues.enumClass();

//TODO add param cast acceptable
        if (!classEnum.isEnum() || classEnum.getSimpleName().equals("Enum")) {
            throw new Exception("test Enum List");
        }

        List<Object> tmp = Arrays.asList(classEnum.getEnumConstants());
        for (Object e : tmp) {
            acceptedValues.add(e.toString());
        }
    }

    @Override
    public boolean isValid(CharSequence value, ConstraintValidatorContext context) {
        if (value == null) {
            return true;
        }

        return acceptedValues.contains(value.toString());
    }
}