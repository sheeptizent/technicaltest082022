package cohou.jeremie.technicalTest.model.validator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation @BirthDateValidator(legalAge = [int], dateFormat = [String])
 * @Param legalAge => age de la majorité légale
 * @Param dateFormat => format valide de la date
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@Constraint(validatedBy = BirthDateValidatorLogic.class)
public @interface BirthDateValidator {

    String message() default "doit être une date de naissance valide au format {dateFormat} d'une personne majeure ({legalAge} ans)";

    int legalAge() default 18;

    String dateFormat() default "dd/MM/yyyy";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

}
