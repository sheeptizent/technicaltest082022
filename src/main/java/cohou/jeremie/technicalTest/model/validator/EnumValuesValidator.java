package cohou.jeremie.technicalTest.model.validator;


import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;
import java.util.List;

/**
 * Annotation @ValueOfList{arrayAllowed = [[]String]}
 * @Param arrayAllowed => tableau de String valide
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = EnumValuesValidatorLogic.class)
public @interface EnumValuesValidator {

    String message() default "doit prendre l'une des valeurs suivantes: {enumClass}";

    Class<?> enumClass();

    String returnMessage() default "";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
