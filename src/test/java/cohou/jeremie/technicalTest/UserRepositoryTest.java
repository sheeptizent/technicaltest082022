package cohou.jeremie.technicalTest;

import cohou.jeremie.technicalTest.model.entity.Country;
import cohou.jeremie.technicalTest.model.entity.Gender;
import cohou.jeremie.technicalTest.model.entity.User;
import cohou.jeremie.technicalTest.repository.UserRepository;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.Rollback;

@DataJpaTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class UserRepositoryTest {

    @Autowired
    private UserRepository userRepository;

    @Test
    @Order(1)
    @Rollback(value = false)
    public void saveUserTest() {

        User userToSave = User.builder()
                .country(Country.France)
                .name("Jérémie")
                .birthDate("11/09/1986")
                .phoneNumber("0674772106")
                .gender(Gender.Homme).build();

        userRepository.save(userToSave);

        User userToSave2 = User.builder()
                .country(Country.France)
                .name("Jérémie")
                .birthDate("11/09/1986")
                .phoneNumber("0674772106")
                .gender(Gender.Homme).build();

        userRepository.save(userToSave2);

        User userToSave3 = User.builder()
                .country(Country.France)
                .name("Jérémie")
                .birthDate("11/09/2021")
                .phoneNumber("0674772106")
                .gender(Gender.Homme).build();

        Assertions.assertThat(userToSave.getId()).isGreaterThan(0L);
        Assertions.assertThat(userToSave2.getId()).isEqualTo(2L);
        Assertions.assertThat(userToSave3.getId()).isEqualTo(null);
    }

    @Test
    @Order(2)
    public void getUserByIdTest() {
        User user = userRepository.findById(1L).orElse(null);
        Assertions.assertThat(user.getId()).isEqualTo(1L);

        User user2 = userRepository.findById(3L).orElse(null);
        Assertions.assertThat(user2).isEqualTo(null);
    }

}

