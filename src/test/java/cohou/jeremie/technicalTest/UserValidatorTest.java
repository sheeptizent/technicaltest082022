package cohou.jeremie.technicalTest;

import cohou.jeremie.technicalTest.model.entity.Country;
import cohou.jeremie.technicalTest.model.entity.Gender;
import cohou.jeremie.technicalTest.model.restBean.UserRestBean;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.util.*;

import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
public class UserValidatorTest {

    @Autowired
    private Validator validator;

    @Test
    public void testUsertKoConstraints() {

        UserRestBean userRestBean = new UserRestBean();
        userRestBean.setName("Jérémie");
        userRestBean.setPhoneNumber("toto");
        userRestBean.setGender(Gender.Homme);
        userRestBean.setBirthDate("12/12/2030");
        userRestBean.setCountry(Country.France);
        Set<ConstraintViolation<UserRestBean>> violations = validator.validate(userRestBean);

        Map<String, String> errors = new HashMap<>();
        violations.forEach((c) -> {
            String fieldName = c.getPropertyPath().toString();
            String message = c.getMessage();
            errors.put(fieldName, message);
        });

        assertTrue(!violations.isEmpty());
        Assertions.assertEquals(errors.get("phoneNumber"), "doit comprendre 10 chiffres et commencer par 0");
        Assertions.assertEquals(errors.get("birthDate"), "doit être une date de naissance valide au format dd/MM/yyyy d'une personne majeure (18 ans)");

    }

    @Test
    public void testUserKoNullable() {

        UserRestBean userRestBean = new UserRestBean();
        Set<ConstraintViolation<UserRestBean>> violations = validator.validate(userRestBean);

        Map<String, String> errors = new HashMap<>();
        violations.forEach((c) -> {
            String fieldName = c.getPropertyPath().toString();
            String message = c.getMessage();
            errors.put(fieldName, message);
        });

        assertTrue(!violations.isEmpty());
        assertTrue(errors.get("name").equals( "ne doit pas être nul"));
        assertTrue(errors.get("birthDate").equals("ne doit pas être nul"));
        assertTrue(errors.get("country").equals("ne doit pas être nul"));

    }

    @Test
    public void testUsertOK(){

        UserRestBean userRestBean = new UserRestBean();
        userRestBean.setName("Jérémie");
        userRestBean.setPhoneNumber("0674772106");
        userRestBean.setGender(Gender.Homme);
        userRestBean.setBirthDate("11/09/1986");
        userRestBean.setCountry(Country.France);

        Set<ConstraintViolation<UserRestBean>> violations = validator.validate(userRestBean);
        assertTrue(violations.isEmpty());
    }

}
