package cohou.jeremie.technicalTest;

import cohou.jeremie.technicalTest.exeptionHandler.ResourceNotFoundException;
import cohou.jeremie.technicalTest.model.entity.Country;
import cohou.jeremie.technicalTest.model.entity.Gender;
import cohou.jeremie.technicalTest.model.entity.User;
import cohou.jeremie.technicalTest.repository.UserRepository;
import cohou.jeremie.technicalTest.service.model.UserService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Optional;

import static java.lang.String.format;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;

@SpringBootTest
public class UserServiceTest {

    @Autowired
    private UserService userService;

    @MockBean
    private UserRepository userRepository;

    @Test
    void testFindById() {
        User userStored = returnUser();
        doReturn(Optional.of(userStored)).when(userRepository).findById(1L);

        User returnedUser = userService.findById(1L);

        Assertions.assertTrue(returnedUser != null);
        Assertions.assertSame(returnedUser, userStored);
    }

    @Test
    void testFindByIdNoRessource() {

        assertThatThrownBy(() -> userService.findById(0L)).isInstanceOf(ResourceNotFoundException.class);

    }

    @Test
    void testSave() {
        User userToSore = returnUser();
        doReturn(userToSore).when(userRepository).save(any());

        User returnedUser = userService.save(any());

        Assertions.assertNotNull(returnedUser);
        Assertions.assertSame(userToSore, returnedUser);
    }

    private User returnUser() {
        User userSaved = User.builder()
                .country(Country.France)
                .name("Jérémie")
                .birthDate("11/09/1986")
                .phoneNumber("0674772106")
                .gender(Gender.Homme)
                .id(1L).build();;
        return userSaved;
    }

}
