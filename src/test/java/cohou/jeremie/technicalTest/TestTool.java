package cohou.jeremie.technicalTest;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

public class TestTool {

    public static String toStringObject(Object o) throws JsonProcessingException {
        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();

        String stringObj = "";
        try {
            stringObj = ow.writeValueAsString(o);
        } catch (JsonProcessingException e) {
            throw e;
        }

        return stringObj;
    }

}
