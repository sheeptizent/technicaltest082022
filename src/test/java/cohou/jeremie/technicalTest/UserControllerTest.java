package cohou.jeremie.technicalTest;

import cohou.jeremie.technicalTest.controller.model.UserController;
import cohou.jeremie.technicalTest.exeptionHandler.ApiExceptionHandler;
import cohou.jeremie.technicalTest.mapper.UserMapper;
import cohou.jeremie.technicalTest.mapper.UserMapperImpl;
import cohou.jeremie.technicalTest.model.entity.Country;
import cohou.jeremie.technicalTest.model.entity.Gender;
import cohou.jeremie.technicalTest.model.entity.User;
import cohou.jeremie.technicalTest.repository.UserRepository;
import net.minidev.json.JSONObject;
import org.assertj.core.api.StringAssert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.internal.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.ResultHandler;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.MethodArgumentNotValidException;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
public class UserControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    UserRepository userRepository;

    @Autowired
    UserController userController;

    @Mock
    private UserMapper mockUserMapper;

    @BeforeEach
    void setUp() {
        this.mockUserMapper = new UserMapperImpl();
        this.mockMvc = MockMvcBuilders.standaloneSetup(userController).setControllerAdvice(ApiExceptionHandler.class).build();
    }

    @Test
    public void testSave() throws Exception {

        JSONObject userJson = returnUserJson();

        ResultActions result = mockMvc.perform(post("/POST/user")
                .characterEncoding("UTF-8")
                .contentType(MediaType.APPLICATION_JSON)
                .content(userJson.toJSONString()));

        result.andExpect(status().isCreated())
                .andExpect(content().json(userJson.toJSONString()));

        Assertions.assertTrue(null == result.andReturn().getResolvedException());
    }

    @Test
    public void testGetById() throws Exception {

        User userStored = returnUser();

        userRepository.save(userStored);

        ResultActions result = mockMvc.perform(get("/GET/user/1"));

        JSONObject userJson = returnUserJson();
        String userString = userJson.toString();

        result.andExpect(status().isOk())
                .andExpect(content().json(userString));

        Assertions.assertTrue(null == result.andReturn().getResolvedException());
    }

    @Test
    public void testGetByIdNotFound() throws Exception {

        ResultActions result = mockMvc.perform(get("/GET/user/0"));

        result.andExpect(status().isNotFound());

        Assertions.assertTrue(null != result.andReturn().getResolvedException());
    }

    @Test
    public void testSaveNotValidatedCountry() throws Exception {

        JSONObject userJson = new JSONObject();
        userJson.put("name", "Jérémie");
        userJson.put("birthDate", "11/09/1986");
        userJson.put("country", "Espagne");

        ResultActions resultAction = this.mockMvc.perform(post("/POST/user")
                .characterEncoding("UTF-8")
                .contentType(MediaType.APPLICATION_JSON)
                .content(userJson.toJSONString()));

        Exception ex = resultAction.andReturn().getResolvedException();
        Assertions.assertTrue(null != ex);
        String messageException = ex.getMessage();
        Assertions.assertTrue(messageException.contains("Espagne") && messageException.contains("[France]"));

    }

    @Test
    public void testSaveNotValidatedGender() throws Exception {

        JSONObject userJson = new JSONObject();
        userJson.put("name", "Jérémie");
        userJson.put("birthDate", "11/09/1986");
        userJson.put("gender", "Pizza");

        ResultActions resultAction = this.mockMvc.perform(post("/POST/user")
                .characterEncoding("UTF-8")
                .contentType(MediaType.APPLICATION_JSON)
                .content(userJson.toJSONString()));

        Exception ex = resultAction.andReturn().getResolvedException();
        Assertions.assertTrue(null != ex);
        String messageException = ex.getMessage();
        Assertions.assertTrue(messageException.contains("Pizza") && messageException.contains("[Homme, Femme]"));
    }

    @Test
    public void testSaveNotValidatedConstraintViolated() throws Exception {

        JSONObject userJson = new JSONObject();
        userJson.put("name", "Jérémie");
        userJson.put("birthDate", "11/09/2020");
        userJson.put("gender", "Homme");

        ResultActions resultAction = this.mockMvc.perform(post("/POST/user")
                .characterEncoding("UTF-8")
                .contentType(MediaType.APPLICATION_JSON)
                .content(userJson.toJSONString()));

        Exception ex = resultAction.andReturn().getResolvedException();
        Assertions.assertTrue(null != ex);
        String messageException = ex.getMessage();
        Assertions.assertTrue(messageException.contains("birthDate") && messageException.contains("doit être une date de naissance valide au format dd/MM/yyyy d'une personne majeure (18 ans)"));
    }

    private User returnUser() {
        User user = new User();
        user.setCountry(Country.France);
        user.setName("Jérémie");
        user.setBirthDate("11/09/1986");
        user.setPhoneNumber("0674772106");
        user.setGender(Gender.Homme);
        return user;
    }

    private JSONObject returnUserJson() {
        JSONObject userJson = new JSONObject();
        userJson.put("name", "Jérémie");
        userJson.put("country", "France");
        userJson.put("birthDate", "11/09/1986");
        userJson.put("phoneNumber", "0674772106");
        userJson.put("gender", "Homme");
        return userJson;
    }

}
