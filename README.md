***** Test Technique *****

** 1 ** Mise en route de spring:
1/ Ne pas oublier de télécharger les dépendances
2/ La classe principale de l'application: cohou.jeremie.technicalTest.TechnicalTestApplication

** 2 ** Vérifier que la BDD in memory H2 est lancée:
1/ Ouvrir http://localhost:9090/h2 dans un navigateur
2/ Vérifier que le champ "JDBC URL" est bien renseigné avec la valeur: jdbc:h2:mem:dataBaseTest
3/ CLiquer sur "Test Connection" ou connectez vous en cliquant sur "Connect"

** 3 ** Collection Postman
La collection PostMan se trouve dans le dossier PostManCollection à la racine du projet.
    - POST /GET/user/{id}
    - GET /POST/user

Cordialement,
Jérémie - jeremie.cohou@gmail.com -



